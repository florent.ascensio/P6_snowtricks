<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Repository\TrickRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    protected $trickRepository;
    protected $userRepository;


    public function __construct(TrickRepository $trickRepository, UserRepository $userRepository)
    {
        $this->trickRepository = $trickRepository;
        $this->userRepository = $userRepository;
    }


    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_US');

        $tricks = $this->trickRepository->findAll();
        $users = $this->userRepository->findAll();

        foreach ($tricks as $trick) {
            // fake message
            for ($commentCount = 0; $commentCount < mt_rand(15 , 20); $commentCount++) {
                $comment = new Comment();
                $comment
                    ->setUser($faker->randomElement($users))
                    ->setTrick($trick)
                    ->setContent($faker->paragraph(mt_rand(1, 4)))
                    ->setCreatedAt($faker->dateTimeBetween($trick->getCreatedAt()))
                    ->setUpdatedAt($faker->dateTimeBetween($trick->getUpdatedAt()));

                $manager->persist($comment);
            }
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            TrickFixture::class
        ];
    }
}