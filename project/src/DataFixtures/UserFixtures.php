<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    protected $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        for ($count = 0; $count < 4; $count++) {
            $user = new User();
            $hash = $this->encoder->encodePassword($user,'password');
            $user->setEmail("user$count@email.com")
                ->setFullname($faker->name())
                ->setPassword($hash)
                ->setCreatedAt(new \DateTime('NOW'))
                ->setUpdatedAt(new \DateTime('NOW'));

            $manager->persist($user);
        }

        $manager->flush();
    }

}