<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Trick;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\SluggerInterface;
use Faker;

class TrickFixture extends Fixture
{

    protected $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    protected function fetchCategories(): array
    {
        $categoryNames = [
            'straight air',
            'grab',
            'spin',
            'flips and inverted rotations',
            'inverted hand plants',
            'slide',
            'stall',
            'tweaks and variations',
            'other'
        ];

        $categories = [];

        foreach ($categoryNames as $name) {
            $category = new Category();
            $category
                ->setName($name)
                ->setSlug($this->slugger->slug($category->getName()))
                ->setCreatedAt(new \DateTime('NOW'));


            $categories[] = $category;
        }

        return $categories;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $categories = $this->fetchCategories();
        foreach ($categories as $category) {

            $manager->persist($category);

            for ($count=0; $count < mt_rand(1,4); $count++){
                $trick = new Trick();
                $trick->setName($faker->name())
                    ->setCategory($category)
                    ->setDescription($faker->paragraph())
                    ->setSlug($faker->slug())
                    ->setCreatedAt($faker->dateTime())
                    ->setUpdatedAt($faker->dateTime());

                $manager->persist($trick);
            }

        }

        $manager->flush();
    }
}




